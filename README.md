# README

The satelligence story map is a custom themed [reveal.js](https://github.com/hakimel/reveal.js) page.

- The story is defined in `index.html`. 
- It uses Reveals iframe background to display maps along with the story. The maps are seperate pages in `maps/`. The story communicates one-way with the maps by sending actions using [iframe.postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage) check out `index.html` and the bottom of `maps/base.html` to see how.
- It uses a cutom satelligence-story-map theme with the content aligned at the top left. The theme is written in sass which can be found in `css/theme/source` do not edit the compiled css in `css/theme`.

The maps are made with leaflet and use layers uploaded to AWS. Common commands:

- `gdal2tiles.py -z 5-14 -r near <input tif file> <output tms directory>`
- `aws s3 cp --recursive <tms directory> s3://s11-public/tms/Bd_hoar_flood/crop_loss`
